extends RigidBody3D
var move : Vector3 = Vector3(0,0,0)
var planet_path : NodePath;
var planet: StaticBody3D = null;
var gravity: float = 0.800916
var gravity_force : Vector3 = Vector3(0,0,0)
var world_mass: Array[int] = [460600, 342320, 235800]
var transports: Array[StringName] = [StringName("transport/"), StringName("transport2/")]
var ball_mass: int = 2
var planet_mass: int = world_mass[0]
var purse_count = 0
func _ready():
	planet_path = NodePath("/root/Environment/planet")
	planet = get_node(planet_path);
	set_physics_process(true);
	set_use_custom_integrator(false)
	
	for i in range(transports.size()):
		get_node("/root/Environment/" + String(transports[i])).position = (get_node("/root/Environment/planet").position+get_node("/root/Environment/world").position)/2
		get_node("/root/Environment/" + String(transports[i]) + "mesh").mesh.height = (get_node("/root/Environment/planet").position-get_node("/root/Environment/world").position).length()
		get_node("/root/Environment/" + String(transports[i]) + "collision").shape.height = (get_node("/root/Environment/planet").position-get_node("/root/Environment/world").position).length()
		get_node("/root/Environment/" + String(transports[i])).look_at(get_node("/root/Environment/planet").global_transform.origin, Vector3(0,1,0))
		

func _integrate_forces(state):
	planet = get_node(planet_path);
	var dt : float = state.get_step()
	gravity_force = gravity * (planet_mass*ball_mass) / sqrt(pow(planet.position.distance_to(self.position),2)) * (planet.position-self.position).normalized()
	#var velocity = state.get_linear_velocity()
	get_node("/root/Environment/cambase").look_at(planet.global_transform.origin, Vector3(0,1,0))
	state.angular_velocity = (get_node("/root/Environment/cambase").transform.basis.x*move.x+get_node("/root/Environment/cambase").transform.basis.y*move.y)*550*PI*dt
	constant_force = (gravity_force * dt)
	if Input.is_action_pressed("ui_select"):
		if get_colliding_bodies().size() != 0:
			constant_force = (-120000*2*(planet.position-self.position).normalized()*dt)

func _physics_process(_delta):
	get_node("/root/Environment/cambase").position = self.position - 10*(planet.position-self.position).normalized()
	move.z = 1
	if Input.is_action_pressed("ui_up"):
		if move.x > -2:
			move.x = move.x-0.04
	elif Input.is_action_pressed("ui_down"):
		if move.x < 2:
			move.x = move.x+0.04
	else:
		move.x = 0
	if Input.is_action_pressed("ui_right"):
		if move.y < 2:
			move.y = move.y+0.04
	elif Input.is_action_pressed("ui_left"):
		if move.y > -2:
			move.y = move.y-0.04
	else:
		move.y = 0


func purse():
	purse_count += 1
	get_node("/root/Environment/UI/RichTextLabel").set_bbcode("[img]purse.png[/img][font=font/font.res] " + str(purse_count) + "/10[/font]")

func _on_transport_body_entered(body):
	transport (body, "planet", "world");

func _on_transport2_body_entered(body):
	transport(body, "planet", "moon");

func transport(body, planet1 : String, planet2 : String):
	var id = get_node("/root/Environment/ball").get_instance_id()
	var type = get_node("/root/Environment/ball").get_class()
	var node_name = get_node("/root/Environment/ball").name
	if str(body) == str(node_name) + ":[" + type + ":" + str(id) + "]":
		if planet == get_node("/root/Environment/" + planet1):
			planet_path = NodePath("/root/Environment/" + planet2)
			planet_mass = world_mass[2]
		else:
			planet_path = NodePath("/root/Environment/" + planet1)
			planet_mass = world_mass[0]

func _on_purse_body_entered(_body):
	purse()
	get_node("/root/Environment/purse").queue_free()

func _on_purse2_body_entered(_body):
	purse()
	get_node("/root/Environment/purse2").queue_free()


func _on_purse3_body_entered(_body):
	purse()
	get_node("/root/Environment/purse3").queue_free()


func _on_purse4_body_entered(_body):
	purse()
	get_node("/root/Environment/purse4").queue_free()


func _on_purse5_body_entered(_body):
	purse()
	get_node("/root/Environment/purse5").queue_free()


func _on_purse6_body_entered(_body):
	purse()
	get_node("/root/Environment/purse6").queue_free()

func _on_purse7_body_entered(_body):
	purse()
	get_node("/root/Environment/purse7").queue_free()

func _on_purse8_body_entered(_body):
	purse()
	get_node("/root/Environment/purse8").queue_free()


func _on_purse9_body_entered(_body):
	purse()
	get_node("/root/Environment/purse9").queue_free()


func _on_purse10_body_entered(_body):
	purse()
	get_node("/root/Environment/purse10").queue_free()



