# Solus-Sphere - planet game

[![Godot 4.0.alpha](Godot-v4.0.svg)](https://downloads.tuxfamily.org/godotengine/4.0/)
[![MIT license](License-MIT-blue.svg)](LICENSE)

This is the "official" repository of the Solus Sphere Project (made with Godot 4.0).

You navigate a ball on different planets and complete levels.

![Screenshot](source/screenshot.png)